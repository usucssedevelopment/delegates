﻿using System;
using System.Windows.Forms;

namespace Delegates
{
    public partial class MainForm : Form
    {
        private RandomColorChanger _randomColorChanger;
        private NumericColorDisplay _numericColorDisplay;
        private GraphicalColorDisplay _graphicalColorDisplay;

        public MainForm()
        {
            InitializeComponent();
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            if (_randomColorChanger != null)
                stopButton_Click(sender, e);

            _numericColorDisplay = new NumericColorDisplay();
            _numericColorDisplay.Show();

            _graphicalColorDisplay = new GraphicalColorDisplay();
            _graphicalColorDisplay.Show();

            _randomColorChanger = new RandomColorChanger();
            _randomColorChanger.ColorChanged += _numericColorDisplay.DisplayColor;
            _randomColorChanger.ColorChanged += _graphicalColorDisplay.DisplayColor;
            _randomColorChanger.Start();
        }

        private void stopButton_Click(object sender, EventArgs e)
        {
            _randomColorChanger.ColorChanged -= _numericColorDisplay.DisplayColor;
            _randomColorChanger.ColorChanged -= _graphicalColorDisplay.DisplayColor;
            _randomColorChanger.Stop();
            _randomColorChanger = null;

            _numericColorDisplay.Close();
            _graphicalColorDisplay.Close();
        }
    }
}
