# Delegates Example

This repository contains variations of a simple color-changing display system that
illustrates delegates and events.  All the variations do the same basic thing, which
is to start up a random color changer and display that random is two different ways
(numerically and graphically).  The variation different in the mechanisms used to
update the two displays.

* Variation 01 - Poor coupling, where random color changer tries to call DisplayColor methods directly
* Variation 02 - Uses delegates to remove the dependency from the random color changer on the DisplayColor methods
* Variation 03 - Uses Events to eliminate dependency on a fixed number of displays
