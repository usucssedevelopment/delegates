﻿namespace Delegates
{
    partial class NumericColorDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.redLabel = new System.Windows.Forms.Label();
            this.greenLabel = new System.Windows.Forms.Label();
            this.blueLabel = new System.Windows.Forms.Label();
            this.blue = new System.Windows.Forms.Label();
            this.green = new System.Windows.Forms.Label();
            this.red = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // redLabel
            // 
            this.redLabel.AutoSize = true;
            this.redLabel.Location = new System.Drawing.Point(26, 25);
            this.redLabel.Name = "redLabel";
            this.redLabel.Size = new System.Drawing.Size(30, 13);
            this.redLabel.TabIndex = 0;
            this.redLabel.Text = "Red:";
            // 
            // greenLabel
            // 
            this.greenLabel.AutoSize = true;
            this.greenLabel.Location = new System.Drawing.Point(26, 51);
            this.greenLabel.Name = "greenLabel";
            this.greenLabel.Size = new System.Drawing.Size(39, 13);
            this.greenLabel.TabIndex = 1;
            this.greenLabel.Text = "Green:";
            // 
            // blueLabel
            // 
            this.blueLabel.AutoSize = true;
            this.blueLabel.Location = new System.Drawing.Point(26, 81);
            this.blueLabel.Name = "blueLabel";
            this.blueLabel.Size = new System.Drawing.Size(31, 13);
            this.blueLabel.TabIndex = 2;
            this.blueLabel.Text = "Blue:";
            // 
            // blue
            // 
            this.blue.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.blue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.blue.Location = new System.Drawing.Point(85, 81);
            this.blue.Name = "blue";
            this.blue.Size = new System.Drawing.Size(59, 16);
            this.blue.TabIndex = 5;
            this.blue.Text = "Blue:";
            // 
            // green
            // 
            this.green.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.green.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.green.Location = new System.Drawing.Point(85, 51);
            this.green.Name = "green";
            this.green.Size = new System.Drawing.Size(59, 16);
            this.green.TabIndex = 4;
            this.green.Text = "Green:";
            // 
            // red
            // 
            this.red.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.red.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.red.Location = new System.Drawing.Point(88, 24);
            this.red.Name = "red";
            this.red.Size = new System.Drawing.Size(56, 16);
            this.red.TabIndex = 3;
            this.red.Text = "Red:";
            // 
            // NumericColorDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(170, 119);
            this.Controls.Add(this.blue);
            this.Controls.Add(this.green);
            this.Controls.Add(this.red);
            this.Controls.Add(this.blueLabel);
            this.Controls.Add(this.greenLabel);
            this.Controls.Add(this.redLabel);
            this.Name = "NumericColorDisplay";
            this.Text = "NumericColorDisplay";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label redLabel;
        private System.Windows.Forms.Label greenLabel;
        private System.Windows.Forms.Label blueLabel;
        private System.Windows.Forms.Label blue;
        private System.Windows.Forms.Label green;
        private System.Windows.Forms.Label red;
    }
}