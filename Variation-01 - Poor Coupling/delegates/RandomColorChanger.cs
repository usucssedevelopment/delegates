﻿using System;
using System.Drawing;
using System.Threading;

namespace Delegates
{
    public class RandomColorChanger
    {
        private static readonly ColorValueRange[] Ranges =
        {
            new ColorValueRange() {Low = 0, High = 100},
            new ColorValueRange() {Low = 100, High = 200},
            new ColorValueRange() {Low = 200, High = 256}
        };

        private static int _changeInterval = 5000;
        private static int _deltaInterval = 50;
        private static readonly float MaxDeltaSteps = Convert.ToSingle(_changeInterval)/Convert.ToSingle(_deltaInterval);

        private static readonly Random RandomGenerator = new Random();

        private Color _originalColor;
        private Color _currentColor;
        private Color _targetColor;
        private float _redDelta;
        private float _greenDelta;
        private float _blueDelta;
        private int _stepCount;
        private int _currentRangeIndex;

        private bool _keepGoing;
        private Thread _myThread;

        public NumericColorDisplay Display1 { get; set; }
        public GraphicalColorDisplay Display2 { get; set; }

        public RandomColorChanger()
        {
            _currentColor = ChooseRandomColor();
        }

        public void Start()
        {
            _keepGoing = true;
            _myThread = new Thread(Run);
            _myThread.Start();
        }

        public void Stop()
        {
            _keepGoing = false;
            _myThread.Join();
        }

        private void Run()
        {
            SetupTargetColor();
            while (_keepGoing)
            {
                Display1?.DisplayColor(_currentColor);
                Display2?.DisplayColor(_currentColor);

                Thread.Sleep(_deltaInterval);
                _stepCount++;

                AdjustCurrentColor();
                if (_stepCount >= MaxDeltaSteps)
                    SetupTargetColor();
            }            
        }

        private void SetupTargetColor()
        {
            _originalColor = Color.FromArgb(_currentColor.R, _currentColor.G, _currentColor.B);
            _targetColor = ChooseRandomColor();
            _redDelta = Convert.ToSingle(_targetColor.R - _currentColor.R)/MaxDeltaSteps;
            _greenDelta = Convert.ToSingle(_targetColor.G - _currentColor.G) / MaxDeltaSteps;
            _blueDelta = Convert.ToSingle(_targetColor.B - _currentColor.B) / MaxDeltaSteps;
            _stepCount = 0;
        }

        private void AdjustCurrentColor()
        {
            var red = AdjustColorValue(_originalColor.R, _stepCount, _redDelta);
            var green = AdjustColorValue(_originalColor.G, _stepCount, _greenDelta);
            var blue = AdjustColorValue(_originalColor.B, _stepCount, _blueDelta);
            _currentColor = Color.FromArgb(red, green, blue);
        }

        private static int AdjustColorValue(int originalValue, int step, float delta)
        {
            var newValue = originalValue + Convert.ToSingle(step)*delta;
            return Math.Max(0, Math.Min(255, Convert.ToInt32(newValue)));
        }

        private Color ChooseRandomColor()
        {
            var redIndex = _currentRangeIndex % Ranges.Length;
            var greenIndex = (_currentRangeIndex + 1 ) % Ranges.Length;
            var blueIndex = (_currentRangeIndex + 2) % Ranges.Length;
            var red = RandomGenerator.Next(Ranges[redIndex].Low, Ranges[redIndex].High);
            var green = RandomGenerator.Next(Ranges[greenIndex].Low, Ranges[greenIndex].High);
            var blue = RandomGenerator.Next(Ranges[blueIndex].Low, Ranges[blueIndex].High);

            _currentRangeIndex++;
            return Color.FromArgb(red, green, blue);
        }

        private class ColorValueRange
        {
            public int Low { get; set; }
            public int High { get; set; }
        }
    }
}
