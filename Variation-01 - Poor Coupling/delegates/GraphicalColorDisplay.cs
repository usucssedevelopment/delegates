﻿using System.Drawing;
using System.Windows.Forms;

namespace Delegates
{
    public partial class GraphicalColorDisplay : Form
    {
        public GraphicalColorDisplay()
        {
            InitializeComponent();
        }

        private delegate void DisplayColorHandler(Color color);
        public void DisplayColor(Color color)
        {
            if (IsDisposed) return;

            if (InvokeRequired)
                BeginInvoke(new DisplayColorHandler(DisplayColor), color);
            else
                colorPanel.BackColor = color;
        }
    }
}
