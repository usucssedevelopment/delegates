﻿using System.Drawing;
using System.Windows.Forms;

namespace Delegates
{
    public partial class NumericColorDisplay : Form
    {
        public NumericColorDisplay()
        {
            InitializeComponent();
        }

        private delegate void DisplayColorHandler(Color color);
        public void DisplayColor(Color color)
        {
            if (IsDisposed) return;

            if (InvokeRequired)
                BeginInvoke(new DisplayColorHandler(DisplayColor), color);
            else
            {
                red.Text = color.R.ToString();
                green.Text = color.G.ToString();
                blue.Text = color.B.ToString();
            }
        }
    }
}
