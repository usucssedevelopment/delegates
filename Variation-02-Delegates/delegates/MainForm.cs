﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Delegates
{
    public partial class MainForm : Form
    {
        private RandomColorChanger _randomColorChanger;
        private NumericColorDisplay _numericColorDisplay;
        private GraphicalColorDisplay _graphicalColorDisplay;

        public MainForm()
        {
            InitializeComponent();
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            if (_randomColorChanger != null)
                stopButton_Click(sender, e);

            _numericColorDisplay = new NumericColorDisplay();
            _numericColorDisplay.Show();

            _graphicalColorDisplay = new GraphicalColorDisplay();
            _graphicalColorDisplay.Show();

            _randomColorChanger = new RandomColorChanger() {
                Handler1 = _numericColorDisplay.DisplayColor,
                Handler2 = _graphicalColorDisplay.DisplayColor
            };
            _randomColorChanger.Start();
        }

        private void stopButton_Click(object sender, EventArgs e)
        {
            _randomColorChanger.Handler1 = null;
            _randomColorChanger.Handler2 = null;
            _randomColorChanger.Stop();
            _randomColorChanger = null;

            _numericColorDisplay.Close();
            _graphicalColorDisplay.Close();
        }
    }
}
