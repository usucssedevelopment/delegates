﻿using System.Drawing;

namespace Delegates
{
    public delegate void DisplayColorHandler(Color color);
}
